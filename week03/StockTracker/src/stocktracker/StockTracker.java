/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker;

import stocktracker.stockdatamodel.PhysicalStockItem;
import stocktracker.stockdatamodel.ServiceStockTracker;

/**
 *
 * @author lboyle1
 */
public class StockTracker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PhysicalStockItem objPhysicalItem =
                new PhysicalStockItem("Snuff: A diskworld book by Terry Pratchett", 0);
        
        ServiceStockTracker objVirtualItem =
                new ServiceStockTracker("Wintersmith: A Diskworld eBook by Terry Pratchett", 0);
        
        String strMessage = objPhysicalItem.getName()
                + ", Is in stock = " + objPhysicalItem.isInStock()
                +", Qty in stock: " + objPhysicalItem.getQuantityInStock();
        System.out.println(strMessage);
        
        String strMessage2 = objVirtualItem.getName()
                + ", Is in stock = " + objVirtualItem.isInStock()
                +", Qty in stock: " + objVirtualItem.getQuantityInStock();
        System.out.println(strMessage2);
        
        
    }
    
}
