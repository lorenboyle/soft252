/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author lboyle1
 */
public abstract class HotDrink extends Drink {
    

    
    public void boilWater(){
        System.out.println("Boiling water");
    }
    
    public abstract void addExtra();
    
    public void pourIntoCup(){
        System.out.println("Pouring into cup");
    }
    
    public abstract void brew();

    
}
