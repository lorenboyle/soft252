/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author lboyle1
 */
public class Admin {
    
    
    public static void assignCourse(UniPeople person, Course course){
        person.setCourse(course);
    }
    
    public static void getDeatils(UniPeople person){
        String courseCode;
        if (person.getCourse() == null){
            courseCode = "Unassigned";
        }
        else{
            courseCode = person.getCourse().getCourseCode();
        }
        System.out.println("Name: " + person.name + " ID: " + person.id + "  course: " + courseCode);
    }
}
