/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arahat
 */
public class ProtectSpell implements ICommand{
    private Receiver receiver;
    public ProtectSpell(Receiver receiver) {
        this.receiver = receiver;
    }
    
    @Override
    public void execute() {
        receiver.setProtectReceiver(true);
    }

    @Override
    public void undo() {
        receiver.setProtectReceiver(false);
    }
    
}
