/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author lboyle1
 */
public abstract class Drink implements IDrink{
   
    public Drink(){
        this.makeDrink();
        this.serveDrink();
    }

    public final void serveDrink(){
        System.out.println("Serving drink now");
    }

    
}
