/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arahat
 */
public class Unprotected extends State{
    
    public Unprotected(){
        this.visible = true;
        this.floatInAir = false;
        this.protectReceiver = false;
    }

    @Override
    public void setProtectReceiver(Receiver receiver, boolean protectReceiver) {
        if (protectReceiver == true) {
            receiver.setState(new Protected());
        }
        else{
            printState();
        }
    }

    @Override
    public void setFloatInAir(Receiver receiver, boolean floatInAir) {
        this.floatInAir = floatInAir;
    }

    @Override
    public void setVisible(Receiver receiver, boolean visible) {
        this.visible = visible;
    }

    @Override
    public void printState() {
        System.out.println("I am in Unprotected state.");
        System.out.println("isFloating: " + this.isFloatInAir());
        System.out.println("isVisibile: " + this.isVisible());
        System.out.println("isProtected: " + this.isProtectReceiver());
    }
    
}
