/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker.stockdatamodel;

/**
 *
 * @author lboyle1
 */
public class ServiceStockTracker extends StockItem {
    
    public ServiceStockTracker(String name){
        this.name = name;
    }
    
    public ServiceStockTracker(String name, Integer qty){
        this.name = name;
        this.quantityInStock = qty;
    }

    @Override
    public Boolean isInStock() {
        return true; 
    }
    
    
}
