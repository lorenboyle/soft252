/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author lboyle1
 */
public class LongDrink extends FancyDrink{
    
    @Override
    public void makeDrink(){
        this.addIce();
        this.addSpirit();
        this.addMixer();
        this.addSimpleGarnish();
    }
   
    
    public void addSimpleGarnish(){
        System.out.println("Add simple garnish");
    }
}
