/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;
import java.util.Scanner;

/**
 *
 * @author lboyle1
 */
public class ATM {
    
    private IState state;
    public ATM(IState state){
        this.state = state;
    }
    
    public void setState(IState state) {
        this.state = state;
    }
    
    public void insertCard() {
        state.insertCard(this);
    }
    
    public void ejectCard() {
        state.ejectCard(this);
    }
    
    public void amountEntered(){
        state.amountEntered(this);
    }
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    }
    
}
