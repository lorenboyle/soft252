/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bikepool;

/**
 *
 * @author lboyle1
 */
public abstract class Bicycle {
    
    String description = "Bike";
    
    public String getDescription(){
        return description;
    }
    
    public abstract double cost();
}
