/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;

/**
 *
 * @author lboyle1
 */
public class Card implements IState {


    
    @Override
    public void insertCard(ATM atm) {
        System.out.println("Card has been inserted, next PIN");
        
        atm.setState(new PIN());
    }
    
    @Override
    public void ejectCard(ATM atm) {
        
        
        System.out.println("Card has been ejected");
        
        atm.setState(new NoCard());
    }

    @Override
    public void dispense(ATM atm) {
        atm.setState(this);
    }

    @Override
    public void refill(ATM atm) {
       atm.setState(this); 
    }
    
    @Override
    public void amountEntered(ATM atm) {
        atm.setState(this);
    }
}


