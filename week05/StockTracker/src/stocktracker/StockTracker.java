/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker;


import stocktracker.stockdatamodel.AnObserver;
import stocktracker.stockdatamodel.PhysicalStockItem;
import stocktracker.stockdatamodel.ServiceStockTracker;
import stocktracker.stockdatamodel.StockItem;
import stocktracker.stockdatamodel.StockType;
import utilities.IObserver;
import utilities.ISubject;

/**
 *
 * @author lboyle1
 */
public class StockTracker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("Creating stock items");
        StockItem objPhysical = new PhysicalStockItem("Halo 3", 100);
        StockItem objService = new ServiceStockTracker("Delivery");
        System.out.println("Stock items created");
        System.out.println("Creating an observer and registering with stock items");
        IObserver observer = new AnObserver();
        objPhysical.registerObserver(observer);
        objService.registerObserver(observer);
        System.out.println("Observer created and registered with stock item");
        System.out.println("Changing physicalstock items quantity in stock");
        objPhysical.setQuantityInStock(99);
        System.out.println("Setting delivery service stock items selling price");
        objService.setSellingPrice(5.00);
        
        
        

      
        
        
        if(objPhysical.getItemType() == StockType.PHYSOCALITEM){
            System.out.println("Item 1 is a PHYSICAL stock item");
        } else {
            System.out.println("Item 1 is a SERVICE stock item");
        }
        
        
        if(objService.getItemType() == StockType.PHYSOCALITEM){
            System.out.println("Item 3 is a PHYSICAL stock item");
        } else {
            System.out.println("Item 3 is a SERVICE stock item");
        }
        
/*        PhysicalStockItem objPhysicalItem =
                new PhysicalStockItem("Snuff: A diskworld book by Terry Pratchett", 0);
        
        ServiceStockTracker objVirtualItem =
                new ServiceStockTracker("Wintersmith: A Diskworld eBook by Terry Pratchett", 0);
        
        String strMessage = objPhysicalItem.getName()
                + ", Is in stock = " + objPhysicalItem.isInStock()
                +", Qty in stock: " + objPhysicalItem.getQuantityInStock();
        System.out.println(strMessage);
        
        String strMessage2 = objVirtualItem.getName()
                + ", Is in stock = " + objVirtualItem.isInStock()
                +", Qty in stock: " + objVirtualItem.getQuantityInStock();
        System.out.println(strMessage2);
        
        
    }
    */
    }
}
