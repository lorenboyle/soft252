/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author lboyle1
 */
public class Cocktail extends FancyDrink{
    
    @Override
    public void makeDrink(){
        this.addIce();
        this.addSpirit();
        this.addSpirit();
        this.addMixer();
        this.addFancyGarnish();
    }
    
    public void addFancyGarnish(){
        System.out.println("Adding fancy garnish");
    }
}
