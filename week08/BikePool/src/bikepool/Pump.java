/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bikepool;

/**
 *
 * @author lboyle1
 */
public class Pump extends BikeDecorator{
    
    public Pump(Bicycle bicycle){
        super(bicycle);
    }
    
    @Override
    public String getDescription(){
        return bicycle.getDescription() + ", Pump";
    }
    
    @Override
    public double cost(){
        return bicycle.cost() + 2;
    }
}