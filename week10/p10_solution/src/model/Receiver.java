/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arahat
 */
public class Receiver {
    private State state;
    
    public Receiver(State state){
        this.state = state;
    }

    public void setState(State state){
        this.state = state;
    }
    
    public boolean isProtectReceiver() {
        //return protectReceiver;
        return state.isProtectReceiver();
    }

    public void setProtectReceiver(boolean protectReceiver) {
        //this.protectReceiver = protectReceiver;
        state.setProtectReceiver(this, protectReceiver);
    }

    public boolean isFloatInAir() {
        //return floatInAir;
        return state.isFloatInAir();
    }

    public void setFloatInAir(boolean floatInAir) {
        //this.floatInAir = floatInAir;
        state.setFloatInAir(this, floatInAir);
    }

    public boolean isVisible() {
        //return visible;
        return state.isVisible();
    }

    public void setVisible(boolean visible) {
        //this.visible = visible;
        state.setVisible(this, visible);
    }
    
    public void printState(){
        state.printState();
    }
}
