/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import PubModel.Strategies.BottleStrategy;
import PubModel.Strategies.CocktailStrategy;
import PubModel.Strategies.CoffeeStrategy;
import PubModel.Strategies.DefaultStrategy;
import PubModel.Strategies.IDrinksStrategy;
import PubModel.Strategies.LongDrinkStrategy;
import PubModel.Strategies.TapStrategy;
import PubModel.Strategies.TeaStrategy;

/**
 *
 * @author lboyle1
 */
public class ConcretePubSimulator {
    
        public IDrinksStrategy createDrink(String drinkType){
        
        if(drinkType.equals("Pint")){
            return new TapStrategy();
        }
        else if(drinkType.equals("Bottle")){
            return new BottleStrategy();
        }
        else if(drinkType.equals("Tea")){
            return new TeaStrategy();
        }
        else if(drinkType.equals("Coffee")){
            return new CoffeeStrategy();
        }
        else if(drinkType.equals("Long Drink")){
            return new LongDrinkStrategy();
        }
        else if(drinkType.equals("Cocktail")){
            return new CocktailStrategy();
        }
        else{
            return new DefaultStrategy();
        }
}
}
