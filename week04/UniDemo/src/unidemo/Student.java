/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author lboyle1
 */
public class Student extends UniPeople{
    
    public Student(int id, String name){
        super(id, name);
        this.setId(id);
        this.setName(name); 
        
    }
    
    public void attendClass(){
        System.out.println(this.name + " is attending " + this.getCourse().courseCode + " in room " + this.getCourse().room);
    }
    
    public void doCoursework(){
        if(this.course.courseCode == null){
            System.out.println("No Coursework set");
        }
        else{
            
        
        System.out.println(this.name + " is doing coursework " + this.getCourse().coursework);
    
        }
    }
    
}
