/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicalsession01;

/**
 *
 * @author lboyle1
 */
public class MyHeight {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         double cm = 10;
         double inches;
         double feet;
         
         inches = cm / 2.54;
         feet = inches / 12;
         inches = inches % 12;
         
         System.out.println("" + cm +"cm is " + inches + "\" and " + feet + " in feet");
    }
    
}
