/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;

/**
 *
 * @author lboyle1
 */
public interface IState {
    public void insertCard(ATM atm);
    public void ejectCard(ATM atm);
    public void amountEntered(ATM atm);
    public void dispense(ATM atm);
    public void refill(ATM atm);
}
