/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bikepool;

/**
 *
 * @author lboyle1
 */
public abstract class BikeDecorator extends Bicycle{
    public Bicycle bicycle;
    
    public BikeDecorator (Bicycle bicycle){
        this.bicycle = bicycle;
    }
}
