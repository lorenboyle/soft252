/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker.stockdatamodel;

/**
 *
 * @author lboyle1
 */
public class ServiceStockTracker extends StockItem {
    
    
    public ServiceStockTracker()
    {
        super();
    }
    
    public ServiceStockTracker(String name)
    {
        super(name);
    }
    
    public ServiceStockTracker(String name, Integer qty)
    {
        super(name, 0);
    }

    @Override
    public Boolean isInStock()
    {
        return true; 
    }
    
    @Override
    public StockType getItemType() {
        return StockType.SERVICEITEM;
    }
    
    
}
