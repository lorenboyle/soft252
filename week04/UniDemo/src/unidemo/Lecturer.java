/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author lboyle1
 */
public class Lecturer extends UniPeople implements ITeach{
    
    public Lecturer(int id, String name){
        super(id, name);
        this.setId(id);
        this.setName(name); 
        
    }
    
    @Override
    public void teach(){
        System.out.println(this.name + " is teaching " + this.getCourse().courseCode + " in room " + this.getCourse().room);
    }
    
    @Override
    public void setCourseWork(String coursework){
        this.getCourse().setCoursework(coursework);
        
    }
    
    
    

}
