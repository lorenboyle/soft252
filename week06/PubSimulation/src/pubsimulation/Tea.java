/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubsimulation;

/**
 *
 * @author lboyle1
 */
public class Tea extends HotDrink{
    
    @Override
    public void makeDrink(){
        this.boilWater();
        this.brew();
        this.pourIntoCup();
        this.addExtra();
    }
    
    @Override
    public void brew(){
        System.out.println("Steeping tea in mug");
    }
    
    @Override
    public void addExtra(){
        System.out.println("Adding Lemon");
    }
    
}
