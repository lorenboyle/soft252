/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import model.ICommand;
import model.InivisibilitySpell;
import model.Invoker;
import model.ProtectSpell;
import model.Receiver;
import model.Unprotected;

/**
 *
 * @author arahat
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Receiver Apple = new Receiver(new Unprotected());
        ICommand invisible = new InivisibilitySpell(Apple);
        ICommand protect = new ProtectSpell(Apple);
        Invoker HarryPotter = new Invoker();
        HarryPotter.setCommand(invisible);
        System.out.println("Apple is visible: " + Apple.isVisible());
        System.out.println("Casting invisibility spell...");
        HarryPotter.castSpell();
        
        System.out.println("Apple is visible: " + Apple.isVisible());
        System.out.println("Reversing spell...");
        HarryPotter.undoSpell();
        System.out.println("Apple is visible: " + Apple.isVisible());
        System.out.println("Protecting Apple...");
        HarryPotter.setCommand(protect);
        HarryPotter.castSpell();
        Apple.printState();
        System.out.println("Try invisibility command again...");
        System.out.println("Casting spell...");
        HarryPotter.setCommand(invisible);
        HarryPotter.castSpell();
        
        System.out.println("Apple is visible: " + Apple.isVisible());
        System.out.println("Reversing spell...");
        HarryPotter.undoSpell();
        System.out.println("Apple is visible: " + Apple.isVisible());
        //Apple.printState();
    }
    
}
