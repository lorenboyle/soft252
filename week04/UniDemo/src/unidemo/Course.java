/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author lboyle1
 */
public class Course {
    
    protected String courseCode;
    protected Lecturer teacher;
    protected String coursework;
    protected String room;
    
    public Course(String room, String courseCode){
        this.room = room;
        this.courseCode = courseCode;
    }

    public String getCourseCode() {
        
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        if (courseCode != null){
            
        this.courseCode = courseCode;
        }
    }

    public Lecturer getTeacher() {
        return teacher;
    }

    public void setTeacher(Lecturer teacher) {
        this.teacher = teacher;
    }

    public String getCoursework() {
        return coursework;
    }

    public void setCoursework(String coursework) {
        this.coursework = coursework;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        if (room != null){
        this.room = room;
    }
        
    }
    
}
