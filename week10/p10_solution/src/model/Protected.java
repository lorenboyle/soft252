/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arahat
 */
public class Protected extends State{
    public Protected(){
        this.visible = true;
        this.floatInAir = false;
        this.protectReceiver = true;
    }
    @Override
    public void setProtectReceiver(Receiver receiver, boolean protectReceiver) {
        // Only change state if unprotected spell 
        if (protectReceiver == false) {
            receiver.setState(new Unprotected());
        }
    }

    @Override
    public void setFloatInAir(Receiver receiver, boolean floatInAir) {
        // do nothing
        printState();
    }

    @Override
    public void setVisible(Receiver receiver, boolean visible) {
        // do nothing
        printState();
    }

    @Override
    public void printState() {
        System.out.println("I am in Protected state.");
        System.out.println("isFloating: " + this.isFloatInAir());
        System.out.println("isVisibile: " + this.isVisible());
        System.out.println("isProtected: " + this.isProtectReceiver());
    }
    
}
