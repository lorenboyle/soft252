/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author arahat
 */
public abstract class State {

    protected boolean floatInAir;
    protected boolean visible;
    protected boolean protectReceiver;
    
    public boolean isFloatInAir() {
        return floatInAir;
    }

    public boolean isVisible() {
        return visible;
    }

    public boolean isProtectReceiver() {
        return protectReceiver;
    }
    
    
    public abstract void setProtectReceiver(Receiver receiver, boolean protectReceiver);
    public abstract void setFloatInAir(Receiver receiver, boolean floatInAir);
    public abstract void setVisible(Receiver receiver, boolean visible);
    public abstract void printState();
}
